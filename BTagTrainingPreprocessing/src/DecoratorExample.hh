#ifndef DECORATOR_EXAMPLE_HH
#define DECORATOR_EXAMPLE_HH

#include "xAODJet/JetFwd.h"
#include "xAODBTagging/BTaggingContainerFwd.h"
#include "AthLinks/ElementLink.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a jet with additional information

class DecoratorExample
{
public:
  using BL = ElementLink<xAOD::BTaggingContainer>;
  using BLA = SG::AuxElement::ConstAccessor<BL>;

  DecoratorExample(const std::string& decorator_prefix = "example_",
                   const DecoratorExample::BLA& btagging_link = BLA(
                     "btagginglink"));

  // this is the function that actually does the decoration
  void decorate(const xAOD::Jet& jet) const;
private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.
  SG::AuxElement::Decorator<float> m_deco;

  // link to access the b-tagging object
  BLA m_btagging_link;
};

#endif
