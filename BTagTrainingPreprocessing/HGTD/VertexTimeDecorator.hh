/* 
* Implement of HGTD vertex time interface https://gitlab.cern.ch/atlas-hgtd/SimulationAndPerformance/hgtdvertextimeinterface
*/

#ifndef VERTEX_TIME_DECORATOR_HH
#define VERTEX_TIME_DECORATOR_HH

#include "xAODTracking/TrackParticleContainerFwd.h"
#include "xAODJet/JetFwd.h"
#include "xAODJet/Jet.h"
#include "xAODBTagging/BTaggingUtilities.h"

#include "FlavorTagDiscriminants/BTagTrackIpAccessor.h"
#include "ClusteringHelper.h"

#include "xAODTracking/VertexContainer.h"

#include "TMVA/Reader.h"
#include "TMVA/Tools.h"

#include "src/TrackSelectorConfig.hh"
#include "src/TrackSelector.hh"

class VertexTimeDecorator
{
public:

  VertexTimeDecorator(TrackSelectorConfig = TrackSelectorConfig());
  std::pair<float, float> decorateAll(const xAOD::TrackParticleContainer& tracks, const xAOD::Vertex& pv) const;

private:

  bool passTrackVertexAssociationZ0(const xAOD::TrackParticle& track, const xAOD::Vertex& pv) const;

  std::pair< bool, std::pair<float, float>>
  getVertexTimeAndUnc(const xAOD::TrackParticleContainer& tracks, const xAOD::Vertex& pv) const;

  std::vector<Cluster<const xAOD::TrackParticle *>>
  clusterTracksInTime(TrackSelector::Tracks tracks, double cluster_distance) const;

  float scoreCluster(const Cluster<const xAOD::TrackParticle *> &clusters, const xAOD::Vertex &pv) const;

  float getSumPt2OfCluster(const Cluster<const xAOD::TrackParticle *> &cluster) const;

  std::pair<float, float>
  getZOfCluster(const Cluster<const xAOD::TrackParticle *> &cluster) const;

  std::pair<float, float>
  getDOfCluster(const Cluster<const xAOD::TrackParticle *> &cluster) const;

  std::pair<float, float>
  getOneOverPOfCluster(const Cluster<const xAOD::TrackParticle *> &cluster) const;

  float getPropertyResolutionVariance(const Cluster<const xAOD::TrackParticle *> &cluster, const int index) const;

  float getPropertyResolutionMean(const Cluster<const xAOD::TrackParticle *> &cluster, const int index) const;

  mutable float m_delta_z;
  mutable float m_z_sigma;
  mutable float m_q_over_p;
  mutable float m_q_over_p_sigma;
  mutable float m_d0;
  mutable float m_d0_sigma;
  mutable float m_delta_z_resunits;
  mutable float m_cluster_sumpt2;
  mutable float m_track_z0st;
  mutable float m_track_deltat_sig;

  TMVA::Reader *m_tmva_reader;
  float m_min_bdt_cutvalue = 0.2;

  SG::AuxElement::ConstAccessor<unsigned char> m_acc_hastime;
  SG::AuxElement::ConstAccessor<float> m_acc_time;
  SG::AuxElement::ConstAccessor<float> m_acc_resolution;

  SG::AuxElement::Decorator<unsigned char> m_dec_hast0;
  SG::AuxElement::Decorator<float> m_dec_t0;
  SG::AuxElement::Decorator<float> m_dec_sigmat0;
  SG::AuxElement::Decorator<unsigned char> m_dec_hastimet0;
  SG::AuxElement::Decorator<float> m_dec_deltat;
  SG::AuxElement::Decorator<float> m_dec_time_signif;

  BTagTrackIpAccessor m_acc;
};

#endif
