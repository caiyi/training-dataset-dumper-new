#include "src/SingleBTagAlg.h"
#include "src/SingleBTagElectronAlg.h"
#include "src/TrackSystematicsAlg.h"
#include "src/TriggerJetGetterAlg.h"
#include "src/TriggerBTagMatcherAlg.h"
#include "src/JetMatcherAlg.h"
#include "src/JetSystematicsAlg.h"
#include "src/MCTCDecoratorAlg.h"
#include "src/TriggerVRJetOverlapDecoratorTool.h"
#include "src/ParentLinkDecoratorAlg.h"
#include "src/TruthJetPrimaryVertexDecoratorAlg.h"

#include "src/H5FileSvc.h"

DECLARE_COMPONENT(SingleBTagAlg)
DECLARE_COMPONENT(SingleBTagElectronAlg)
DECLARE_COMPONENT(TrackSystematicsAlg)
DECLARE_COMPONENT(TriggerJetGetterAlg)
DECLARE_COMPONENT(TriggerBTagMatcherAlg)
DECLARE_COMPONENT(JetMatcherAlg)
DECLARE_COMPONENT(JetSystematicsAlg)
DECLARE_COMPONENT(MCTCDecoratorAlg)
DECLARE_COMPONENT(TriggerVRJetOverlapDecoratorTool)
DECLARE_COMPONENT(ParentLinkDecoratorAlg)
DECLARE_COMPONENT(TruthJetPrimaryVertexDecoratorAlg)

DECLARE_COMPONENT(H5FileSvc)
