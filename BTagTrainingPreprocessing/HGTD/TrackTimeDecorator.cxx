/* 
* Implement of HGTD track time interface https://gitlab.cern.ch/atlas-hgtd/SimulationAndPerformance/HGTDTrackTimeInterface
*/

#include "TrackTimeDecorator.hh"

// the constructor just builds the decorator
TrackTimeDecorator::TrackTimeDecorator():
  m_acc_perLayer_hasCluster("HGTD_has_extension"),
  m_acc_perLayer_clusterDeltaT("HGTD_cluster_time"),
  m_acc_perLayer_clusterTruthClassification("HGTD_cluster_truth_class"),
  m_acc_perLayer_expectCluster("HGTD_primary_expected"),
  m_dec_isset("HGTDIsset"),
  m_dec_hastime("HGTDHasTime"),
  m_dec_time("HGTDTime"),
  m_dec_nhits("numberOfHGTDHits"),
  m_dec_nprimehits("numberOfHGTDPrimeHits"),
  m_dec_primefraction("HGTDPrimeFraction"),
  m_dec_resolution("HGTDTimeResolution"),
  m_dec_nexpectedhits("numberOfHGTDExpectedHits")
{
}

// Loop over all tracks
void TrackTimeDecorator::decorateAll(const xAOD::TrackParticleContainer& tracks) const {

  for ( const auto& track : tracks ) {
    decorateTime(*track);
  }

}

bool TrackTimeDecorator::decorateTime(const xAOD::TrackParticle& track_particle) const {

  m_dec_nexpectedhits(track_particle) = numberPotentialPrimaryHits(track_particle);

  // check if the last measurement is close to HGTD
  if (m_do_last_hit and not lastHitIsOnLastSurface(track_particle)) {
    m_dec_isset(track_particle) = true;
    m_dec_hastime(track_particle) = 0;
    m_dec_time(track_particle) = 0.;
    m_dec_nhits(track_particle) = 0;
    m_dec_nprimehits(track_particle) = 0;
    m_dec_resolution(track_particle) = 0.;
    m_dec_primefraction(track_particle) = 0.;
    return false;
  }

  HitVec_t used_hits;
  if (m_do_time_cons) {
    // retrieve the hits that survive the preset cuts
    used_hits = getTimeCompatibleHits(track_particle);
  } else {
    // retrieve all hits, no time compatibility check was required
    used_hits = getValidHits(track_particle);
  }

  // if (m_do_min_nhits and used_hits.size() == 1) {
  //   // if a 2 hit minimum is required, reject the case of a single associated
  //   // hit if the track falls into the defined eta region
  //   float fabs_eta = std::abs(track_particle.eta());
  //   if (fabs_eta > m_min_eta and fabs_eta < m_max_eta) {
  //     m_dec_isset(track_particle) = true;
  //     m_dec_hastime(track_particle) = false;
  //     m_dec_time(track_particle) = -999.;
  //     m_dec_nhits(track_particle) = 0;
  //     m_dec_nprimehits(track_particle) = 0;
  //     m_dec_resolution(track_particle) = -999.;
  //     return false;
  //   }
  // }

  m_dec_isset(track_particle) = true;
  m_dec_hastime(track_particle) = int(used_hits.size() > 0);
  m_dec_time(track_particle) = calculateMean(used_hits);
  m_dec_nhits(track_particle) = used_hits.size();
  m_dec_nprimehits(track_particle) = numberOfPrimaryHits(used_hits);
  m_dec_resolution(track_particle) = calculateTrackResolution(used_hits);
  if (used_hits.size() > 0) {m_dec_primefraction(track_particle) = numberOfPrimaryHits(used_hits)/used_hits.size();}
  else {m_dec_primefraction(track_particle) = 0.;}

  return used_hits.size() > 0;
}

std::vector<TrackTimeDecorator::Hit> TrackTimeDecorator::getTimeCompatibleHits(
    const xAOD::TrackParticle& track_particle) const {

  // get all available hits in a first step
  TrackTimeDecorator::HitVec_t valid_hits = getValidHits(track_particle);

  size_t vts = valid_hits.size();

  // if there is only one hit, no time consistency check can be done
  // to improve efficiency, accept it
  if (vts <= 1) {
    return valid_hits;
  }

  // in case of two hits, check for time compatibility
  if (vts == 2) {
    if (passesDeltaT(valid_hits)) {
      return valid_hits;
    } else {
      // if times are too far away from each other, don't accept time
      return {};
    }
  }
  // if there are 3 or 4 hits, perform chi2 outlier removal
  // calculate the chi2 value of the available hits in a first step
  float chi2 = calculateChi2(valid_hits);

  // if the chi2 value doesn't surpass the set threshold, the hits are accepted
  // as compatible in time
  if (chi2 < m_chi2_threshold) {
    return valid_hits;
  }

  HitVec_t time_candidates_copy = valid_hits; // TODO do I need this copy?
  bool searching = true;
  while (searching) {
    // calculate chi2 contribution of each value
    FloatVec_t chi2_contributions(time_candidates_copy.size(), 0.0);
    for (size_t i = 0; i < time_candidates_copy.size(); i++) {
      HitVec_t buff = time_candidates_copy;
      buff.erase(buff.begin() + i);

      // calculate the chi2 value we would get when removing the i-th hit
      double local_chi2 = calculateChi2(buff);

      chi2_contributions.at(i) = local_chi2;
    }
    // if removing one of the hits gives a much smaller chi2, it should be
    // removed, so find the position where the "local chi2" is the smallest, and
    // this is the hit that should be removed (since it gave a big
    // contribution)]

    // find minimum local chi2
    int position = std::distance(
        chi2_contributions.begin(),
        std::min_element(chi2_contributions.begin(), chi2_contributions.end()));

    // and remove it from the hits
    time_candidates_copy.erase(time_candidates_copy.begin() + position);

    // recompute chi2 value
    chi2 = calculateChi2(time_candidates_copy);

    // check for accepted chi2
    if (chi2 < m_chi2_threshold) {
      // if the threshold is now fulfilled, break out of the while loop
      searching = false;
    }
    // if everything except 2 values has been removed, check again for
    // consistency
    if (time_candidates_copy.size() == 2) {

      if (passesDeltaT(time_candidates_copy)) {
        return time_candidates_copy;
      } else {
        // if times are too far away, don't accept any TODO maybe accept one,
        // can the spatial chi2 be used?
        return {};
      }
    }
  }

  return time_candidates_copy;
}

bool TrackTimeDecorator::passesDeltaT(const HitVec_t& hits) const {
  // WARNING I don't check it here, but the vector has to be of size 2!!!
  // pass if the distance in units of the resolution passes the cut
  if (std::abs(hits.at(0).time - hits.at(1).time) <
      m_deltat_cut * std::hypot(hits.at(0).resolution, hits.at(1).resolution)) {
    return true;
  }
  return false;
}

float TrackTimeDecorator::calculateChi2(const TrackTimeDecorator::HitVec_t& hits) const {
  float mean = calculateMean(hits);

  float chi2 = 0.;
  for (size_t i = 0; i < hits.size(); i++) {
    chi2 += (hits.at(i).time - mean) * (hits.at(i).time - mean) /
            (hits.at(i).resolution * hits.at(i).resolution);
  }
  // TODO should I better use chi2/ndof, where ndof = hits.size() - 1 (due to
  // mean)?
  return chi2;
}

float TrackTimeDecorator::calculateMean(const TrackTimeDecorator::HitVec_t& hits) const {
  // FIXME improve this
  if (hits.size() == 0) {
    return 0.;
  }
  float sum = 0.;
  for (const TrackTimeDecorator::Hit& hit : hits) {
    sum += hit.time;
  }
  return sum / (float)hits.size();
}

float TrackTimeDecorator::calculateMean(const std::vector<float>& vals) const {
  float sum = std::accumulate(vals.begin(), vals.end(), 0.0);
  return sum / (float)vals.size();
}

std::vector<TrackTimeDecorator::Hit>
TrackTimeDecorator::getValidHits(const xAOD::TrackParticle& track_particle) const {

  std::vector<float> times =
      m_acc_perLayer_clusterDeltaT(track_particle);
  std::vector<bool> has_clusters =
      m_acc_perLayer_hasCluster(track_particle);
  std::vector<int> hit_classification =
      m_acc_perLayer_clusterTruthClassification(track_particle);

  HitVec_t valid_hits;
  valid_hits.reserve(4);

  for (size_t i = 0; i < has_clusters.size(); i++) {
    if (not has_clusters.at(i)) {
      continue;
    }
    Hit newhit;
    newhit.time = times.at(i);
    newhit.resolution = 0.035; // nano seconds
    newhit.isprime = hit_classification.at(i) == 1;

    valid_hits.push_back(newhit);
  }
  return valid_hits;
}

bool TrackTimeDecorator::lastHitIsOnLastSurface(
    const xAOD::TrackParticle& track) const {

  TVector3 last_hit = this->getLastMeasurement(track);
  double radius = std::hypot(last_hit.X(), last_hit.Y());
  double abs_z = std::abs(last_hit.Z());

  bool is_last = abs_z > 2700;
  is_last = is_last || (radius < 350 and abs_z > 2400);
  is_last = is_last || (radius > 205 and radius < 350 and abs_z > 2100);
  is_last = is_last || (radius < 220 and abs_z > 2200);
  is_last = is_last || (radius < 140 and abs_z > 1890);
  return is_last;
}

int TrackTimeDecorator::numberOfPrimaryHits(
    const TrackTimeDecorator::HitVec_t& hits) const {
  int n = 0;
  for (const TrackTimeDecorator::Hit& hit : hits) {
    if (hit.isprime) {
      n++;
    }
  }
  return n;
}

float TrackTimeDecorator::calculateTrackResolution(
    const TrackTimeDecorator::HitVec_t& hits) const {
  // should never happen
  if (hits.size() == 0) {
    return 0.;
  }
  float sum = 0;
  for (const TrackTimeDecorator::Hit& hit : hits) {
    sum += 1. / (hit.resolution * hit.resolution);
  }
  return std::sqrt(1. / sum);
}

TVector3
TrackTimeDecorator::getLastMeasurement(const xAOD::TrackParticle& track) const {

  unsigned int index = 0;

  if (not track.indexOfParameterAtPosition(index, xAOD::LastMeasurement)) {
    return TVector3(0, 0, 0);
  }

  return TVector3(track.parameterX(index), track.parameterY(index),
                  track.parameterZ(index));
}

int TrackTimeDecorator::numberPotentialPrimaryHits(const xAOD::TrackParticle& track_particle) const {
  
  std::vector<bool> expected_hits = m_acc_perLayer_expectCluster(track_particle);
  return std::count(expected_hits.begin(), expected_hits.end(), true);
}