/* 
* Implement of HGTD track time interface https://gitlab.cern.ch/atlas-hgtd/SimulationAndPerformance/HGTDTrackTimeInterface
*/

#ifndef TRACK_TIME_DECORATOR_HH
#define TRACK_TIME_DECORATOR_HH

#include "xAODBTagging/BTaggingUtilities.h"
#include "src/TrackSelector.hh"

// HGTD
#include "TVector3.h"
#include <numeric>

class TrackTimeDecorator
{
struct Hit {
    float time;
    float resolution;
    bool isprime;
    TVector3 position;
  };

  using HitVec_t = std::vector<Hit>;
  using FloatVec_t = std::vector<float>;
  
public:
  TrackTimeDecorator();

  // this is the function that actually does the decoration
  void decorateAll(const xAOD::TrackParticleContainer& tracks) const;

private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.

  // for testing, might change to virtual
  HitVec_t getValidHits(const xAOD::TrackParticle& track_particle) const;
  HitVec_t getTimeCompatibleHits(const xAOD::TrackParticle& track_particle) const;
  TVector3 getLastMeasurement(const xAOD::TrackParticle& track) const;
  bool lastHitIsOnLastSurface(const xAOD::TrackParticle& track) const;
  float calculateMean(const std::vector<float>& vals) const;
  float calculateMean(const HitVec_t& hits) const;
  bool passesDeltaT(const HitVec_t& hits) const;
  float calculateChi2(const std::vector<float>& vals) const;
  float calculateChi2(const HitVec_t& vals) const;
  float calculateTrackResolution(const HitVec_t& vals) const;
  int numberOfPrimaryHits(const HitVec_t& vals) const;
  int numberPotentialPrimaryHits(const xAOD::TrackParticle& track) const;

  // should be public?
  virtual bool decorateTime(const xAOD::TrackParticle& track_particle) const;

  bool m_do_last_hit = true;
  bool m_do_time_cons = true;
  float m_deltat_cut = 2.0;     // in units of sigma
  float m_chi2_threshold = 1.5; // default value 1.5
  bool m_do_min_nhits = false;
  float m_min_eta = 3.5;
  float m_max_eta = 3.9;
  bool m_do_smearing = false;

  SG::AuxElement::ConstAccessor<std::vector<bool>> m_acc_perLayer_hasCluster;
  SG::AuxElement::ConstAccessor<std::vector<float>> m_acc_perLayer_clusterDeltaT;
  SG::AuxElement::ConstAccessor<std::vector<int>> m_acc_perLayer_clusterTruthClassification;
  SG::AuxElement::ConstAccessor<std::vector<bool>> m_acc_perLayer_expectCluster;

  SG::AuxElement::Decorator<bool> m_dec_isset;
  SG::AuxElement::Decorator<unsigned char> m_dec_hastime;
  SG::AuxElement::Decorator<float> m_dec_time;
  SG::AuxElement::Decorator<unsigned char> m_dec_nhits;
  SG::AuxElement::Decorator<unsigned char> m_dec_nprimehits;
  SG::AuxElement::Decorator<float> m_dec_primefraction;
  SG::AuxElement::Decorator<float> m_dec_resolution;
  SG::AuxElement::Decorator<unsigned char> m_dec_nexpectedhits;

};

#endif
