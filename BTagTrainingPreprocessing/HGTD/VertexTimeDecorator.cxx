/* 
* Implement of HGTD vertex time interface https://gitlab.cern.ch/atlas-hgtd/SimulationAndPerformance/hgtdvertextimeinterface
*/

#include "VertexTimeDecorator.hh"
#include "PathResolver/PathResolver.h"

// the constructor just builds the decorator
VertexTimeDecorator::VertexTimeDecorator(TrackSelectorConfig config):
  m_acc_hastime("HGTDHasTime"),
  m_acc_time("HGTDTime"),
  m_acc_resolution("HGTDTimeResolution"),
  m_dec_hast0("HGTDHasT0"),
  m_dec_t0("HGTDT0"),
  m_dec_sigmat0("HGTDT0Resolution"),
  m_dec_hastimet0("HGTDHasTimeT0"),
  m_dec_deltat("HGTDTimeT0Delta"),
  m_dec_time_signif("HGTDTimeT0Signif"),
  m_acc(config.ip_prefix)
{
  TMVA::Tools::Instance();
  m_tmva_reader = new TMVA::Reader("!Color:!Silent");
  m_tmva_reader->AddVariable("m_delta_z", &m_delta_z);
  m_tmva_reader->AddVariable("m_z_sigma", &m_z_sigma);
  m_tmva_reader->AddVariable("m_q_over_p", &m_q_over_p);
  m_tmva_reader->AddVariable("m_q_over_p_sigma", &m_q_over_p_sigma);
  m_tmva_reader->AddVariable("m_delta_z_resunits", &m_delta_z_resunits);
  m_tmva_reader->AddVariable("m_cluster_sumpt2", &m_cluster_sumpt2);
  m_tmva_reader->AddVariable("m_d0", &m_d0);
  m_tmva_reader->AddVariable("m_d0_sigma", &m_d0_sigma);
  std::string resolved_tmva_name = PathResolverFindCalibFile("TMVA.VBFinv.mu200.Step3p1.8var.weights.xml");
  m_tmva_reader->BookMVA("BDT", TString(resolved_tmva_name));
}

std::pair<float, float> VertexTimeDecorator::decorateAll(const xAOD::TrackParticleContainer& tracks, const xAOD::Vertex& pv) const {
  std::pair< bool, std::pair<float, float>> returnval_t0;
  returnval_t0 = getVertexTimeAndUnc(tracks, pv);
  float t0 = returnval_t0.first ? returnval_t0.second.first : 0.;
  float sigmat0 = returnval_t0.first ? returnval_t0.second.second : 0.;
  for ( const auto& track : tracks ) {
    m_dec_hast0(*track) = int(returnval_t0.first);
    m_dec_t0(*track) = t0;
    m_dec_sigmat0(*track) = sigmat0;
    m_dec_hastimet0(*track) = int(m_dec_hast0(*track) and m_acc_hastime(*track));
    m_dec_deltat(*track) = m_dec_hastimet0(*track) ? m_acc_time(*track) - m_dec_t0(*track) : 0.;
    m_dec_time_signif(*track) = m_dec_hastimet0(*track) ? m_dec_deltat(*track)/sqrt(pow(m_acc_resolution(*track),2)+pow(m_dec_sigmat0(*track),2)) : 0.;
  }
  std::pair<float, float> output = {t0, sigmat0};
  return output;
}

bool VertexTimeDecorator::passTrackVertexAssociationZ0(const xAOD::TrackParticle& track, const xAOD::Vertex& pv) const {

  double pt = track.pt()/1.e3;
  double eta = std::abs(track.eta());

  if ( pt < 1. || eta > 4. || fabs(track.z0() - pv.z()) > 250 ) return false;
  if ( eta < 2.7 ){
    if ( std::abs(track.d0()) > 2 ) return false;
  }
  else if ( std::abs(track.d0()) > 10 ){
    return false;
  }


  std::vector <double> p_v;

  if( pt <= 1.5) {
    p_v = { 0.0622666, -0.0727512, 0.323766, -0.479161, 0.347524, -0.103268, 0.0111874 };
  }
  if( (pt > 1.5) && (pt <=2.5) ) {
    p_v = { 0.039075, -0.0449015, 0.198707, -0.289311, 0.206221, -0.0607471, 0.00655867 };
  }
  if( (pt > 2.5) && (pt <=5.0) ) {
    p_v = { 0.0267551, -0.0303344, 0.132752, -0.188808, 0.131231, -0.0381548, 0.00409764 };
  }
  if( (pt > 5) && (pt <= 10) ) {
    p_v = { 0.0182236, -0.0206502, 0.0879188, -0.119813, 0.0794155, -0.0224977, 0.00238907 };
  }
  if( (pt > 10) ) {
    p_v = { 0.0136149, -0.0159818, 0.0648501, -0.0833277, 0.051544, -0.0140104, 0.00145876 };
  }

  if(eta<2.5) {eta=2.5;}

  double z0_res_param =  p_v.at(0) + p_v.at(1)*eta + p_v.at(2)*pow(eta,2) +p_v.at(3)*pow(eta,3) + p_v.at(4)*pow(eta,4);
  z0_res_param += p_v.at(5)*pow(eta,5)+p_v.at(6)*pow(eta,6);
  z0_res_param *= 2.5;

  z0_res_param = std::abs(z0_res_param);

  if (std::abs(track.z0() - pv.z()) >= z0_res_param) {
    return false;
  }

  return true;

}

std::pair< bool, std::pair<float, float>> VertexTimeDecorator::getVertexTimeAndUnc (
    const xAOD::TrackParticleContainer& tracks,
    const xAOD::Vertex& pv) const {
      
  TrackSelector::Tracks new_tracks;
  for ( const auto& track : tracks ) {
    if( passTrackVertexAssociationZ0(*track, pv) ) new_tracks.emplace_back(track);
  }

  auto clusters = clusterTracksInTime(new_tracks, 3.0);

  double max_bdt_value = -1;

  std::pair< bool, std::pair<float, float>> res = {false, {-999, -999}};

  for (const auto &cluster : clusters) {
    float bdt_output = scoreCluster(cluster, pv);
    int n_tracks = cluster.getEntries().size();
    if (bdt_output > m_min_bdt_cutvalue and bdt_output > max_bdt_value and n_tracks >= 3) {
      max_bdt_value = bdt_output;
      res = {true, {cluster.getValues().at(0), cluster.getSigmas().at(0)}};
    }
  }
  return res;
}

std::vector<Cluster<const xAOD::TrackParticle *>>
VertexTimeDecorator::clusterTracksInTime(
    TrackSelector::Tracks tracks,
    double cluster_distance) const {

  ClusterCollection<const xAOD::TrackParticle *> collection;
  for (const auto &track : tracks) {
    if (m_acc_hastime(*track)) {
      double time = m_acc_time(*track);
      double resolution = m_acc_resolution(*track);
      Cluster<const xAOD::TrackParticle *> cluster({time}, {resolution}, track);
      collection.addCluster(cluster);
    }
  } // LOOP  tracks
  collection.updateDistanceCut(cluster_distance);
  collection.doClustering(CH::ClusterAlgoType::Simultaneous);
  auto clusters = collection.getClusters();
  return clusters;
}

float VertexTimeDecorator::scoreCluster(
    const Cluster<const xAOD::TrackParticle *> &cluster,
    const xAOD::Vertex& pv) const {

  auto cluster_z = getZOfCluster(cluster);
  m_delta_z = cluster_z.first - pv.z();
  m_z_sigma = cluster_z.second;
  auto cluster_oneover_p = getOneOverPOfCluster(cluster);
  m_q_over_p = cluster_oneover_p.first;
  m_q_over_p_sigma = cluster_oneover_p.second;
  auto cluster_d0 = getDOfCluster(cluster);
  m_d0 = cluster_d0.first;
  m_d0_sigma = cluster_d0.second;

  float vertex_z_sigma = sqrt(pv.covariancePosition()(2, 2));
  m_delta_z_resunits = (cluster_z.first - pv.z()) /
                       sqrt(cluster_z.second * cluster_z.second +
                            vertex_z_sigma * vertex_z_sigma);
  m_cluster_sumpt2 = getSumPt2OfCluster(cluster);
  return m_tmva_reader->EvaluateMVA("BDT");
}

float VertexTimeDecorator::getSumPt2OfCluster(
    const Cluster<const xAOD::TrackParticle *> &cluster) const {
  auto tracks = cluster.getEntries();
  float sumpt2 = 0;
  for (const auto &track : tracks) {
    sumpt2 += track->pt() * track->pt();
  }
  return sumpt2;
}

std::pair<float, float> VertexTimeDecorator::getZOfCluster(
    const Cluster<const xAOD::TrackParticle *> &cluster) const {
  auto tracks = cluster.getEntries();
  float num = 0;
  float denom = 0;
  for (const auto &track : tracks) {
    float z0 = track->z0();
    float z0_sigma = std::sqrt(track->definingParametersCovMatrix()(1, 1));
    num += z0 / (z0_sigma * z0_sigma);
    denom += 1. / (z0_sigma * z0_sigma);
  }
  float avg_z0 = num / denom;
  float avg_z0_sigma = sqrt(1. / denom);
  return {avg_z0, avg_z0_sigma};
}

std::pair<float, float> VertexTimeDecorator::getDOfCluster(
    const Cluster<const xAOD::TrackParticle *> &cluster) const{
  auto tracks = cluster.getEntries();
  float num = 0;
  float denom = 0;
  for (const auto &track : tracks) {
    float d0 = track->d0();
    float d0_sigma = std::sqrt(track->definingParametersCovMatrix()(0, 0));

    num += d0 / (d0_sigma * d0_sigma);
    denom += 1. / (d0_sigma * d0_sigma);
  }
  float avg_z0 = num / denom;
  float avg_z0_sigma = sqrt(1. / denom);
  return {avg_z0, avg_z0_sigma};
}

std::pair<float, float> VertexTimeDecorator::getOneOverPOfCluster(
    const Cluster<const xAOD::TrackParticle *> &cluster) const {
  auto tracks = cluster.getEntries();
  float num = 0;
  float denom = 0;
  for (const auto &track : tracks) {
    float oneover_p = fabs(track->qOverP());
    float q_over_p =
        fabs(std::sqrt(track->definingParametersCovMatrix()(4, 4))); // qoverp

    num += oneover_p / (q_over_p * q_over_p);
    denom += 1. / (q_over_p * q_over_p);
  }
  float avg_oneover_p = num / denom;
  float avg_oneover_p_sigma = sqrt(1. / denom);
  return {avg_oneover_p, avg_oneover_p_sigma};
}

float VertexTimeDecorator::getPropertyResolutionVariance(
    const Cluster<const xAOD::TrackParticle *> &cluster, const int index) const {
  auto tracks = cluster.getEntries();
  std::vector<float> z0res_vec;
  for (const auto &track : tracks) {
    float val_sigma =
        std::sqrt(track->definingParametersCovMatrix()(index, index));
    z0res_vec.push_back(val_sigma);
  }
  return TMath::StdDev(z0res_vec.begin(), z0res_vec.end());
}

float VertexTimeDecorator::getPropertyResolutionMean(
    const Cluster<const xAOD::TrackParticle *> &cluster, const int index) const {
  auto tracks = cluster.getEntries();
  std::vector<float> z0res_vec;
  for (const auto &track : tracks) {
    float val_sigma =
        std::sqrt(track->definingParametersCovMatrix()(index, index));
    z0res_vec.push_back(val_sigma);
  }
  return TMath::Mean(z0res_vec.begin(), z0res_vec.end());
}
