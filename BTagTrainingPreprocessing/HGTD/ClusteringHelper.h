/**
 * Copied from https://gitlab.cern.ch/atlas-hgtd/SimulationAndPerformance/hgtdvertextimeinterface/-/blob/21.9/HGTDVertexTimeInterface/ClusteringHelper.h for HGTD study
 */

#ifndef CLUSTERINGHELPER_H
#define CLUSTERINGHELPER_H

#include "TMath.h"

#include <algorithm>
#include <iostream>
#include <math.h>
#include <memory>
#include <stdexcept>
#include <vector>

template <typename T> class Cluster {

public:
  Cluster();

  Cluster(const std::vector<double> &v, const std::vector<double> &v_sigma)
      : m_combined_value_vector(v), m_combined_sigma_vector(v_sigma) {
    for (const auto &s : v_sigma) {
      if (s < 0) {
        m_contains_unknowns = true;
      }
    }
  }

  Cluster(const std::vector<double> &v, const std::vector<double> &v_sigma,
          const T &entry)
      : m_combined_value_vector(v), m_combined_sigma_vector(v_sigma) {
    m_entries.push_back(entry);
    m_n_entries++;
    for (const auto &s : v_sigma) {
      if (s < 0) {
        m_contains_unknowns = true;
      }
    }
  }

  /**
   * @brief The member variable "m_entries" stores the raw objects that are
   * part of this vertex. This way one can e.g. determine how many objects
   * are clustered within this vertex.
   *
   * @param [in] entry Added object, to be stored in the internal
   * collection.
   *
   */
  void addEntry(const T &entry);

  /**
   * @brief This can be used to combine the subsets stored in two vertices
   * all in once while merging.
   *
   * @param [in] entry_vector Vector of objects (usually extracted from vertex
   * to be merged with).
   *
   */
  void addEntryVector(const std::vector<T> &entry_vector);

  /**
   * @brief Gives back the original objects that are clustered within the
   * current vertex.
   */
  std::vector<T> getEntries() const;

  /**
   * @brief The value of a vertex has to be set manually. This is because
   * we use a weighted time after vertex fusion, so this has to be accounted
   * for (also gives the option to use different methods).
   *
   * @param [in] v Global position of this Cluster.
   * @param [in] v_sigma Global resolution of the position of this vertex.
   *
   */
  void setClusterValue(const std::vector<double> &v,
                       const std::vector<double> &v_sigma);

  /**
   * @brief Returns the (possibly via weighting acchieved) value of this vertex.
   *
   */
  std::vector<double> getValues() const;

  /**
   * @brief Returns the (possibly via weighting acchieved) resolution of this
   * vertex.
   *
   */
  std::vector<double> getSigmas() const;

  /**
   * @brief Returns the number of objects stored in this vertex.
   *
   */
  int getNEntries() const;

  /**
   * @brief Creates a new Cluster object that is a fusion of the current
   * vertex and the one given in the argument
   *
   * @param [in] vx A vertex that should be fused with the current one.
   */
  // Cluster<T> fuseWithAnotherCluster(const Cluster<T>& vx);

  /**
   * @brief If a Cluster is the result of a merge, it gets flagged as such.
   * This can be useful within the clustering algorithm.
   **/
  bool mergeStatus() const;

  void setMergeStatus(bool status);

  int getMergeIteration() const;

  void setMergeIteration(int iteration);

  bool containsUnknowns() const;

  void setUnknownStatus(bool status);

private:
  int m_debug_level = 0; // TODO set this dynamically
  int m_n_entries = 0;
  std::vector<T> m_entries;
  double m_combined_value;
  double m_combined_sigma;
  std::vector<double> m_combined_value_vector;
  std::vector<double> m_combined_sigma_vector;
  bool m_was_merged = false;
  int m_merge_iteration = 0;
  bool m_contains_unknowns = false;
};

/////////////////////////////////////////////
/////////////////////////////////////////////
namespace CH {
enum class ClusterAlgoType { Eager, Simultaneous, SimultaneousMean };
}

template <typename T> class ClusterCollection {
public:
  void addCluster(Cluster<T> vx);
  void doClustering(CH::ClusterAlgoType algo);

private:
  void doEagerClustering();
  void doSimultaneousClustering();
  void doSimultaneousClusteringMean();

  double getDistanceBetweenClusters(const Cluster<T> &a, const Cluster<T> &b);

  /**
   * @brief Creates a new Cluster object that is a fusion of the two clusters
   * given in the arguments.
   *
   * @param [in] a A cluster that should be merged.
   * @param [in] b A cluster that should be merged.
   */
  Cluster<T> mergeClusters(const Cluster<T> &a, const Cluster<T> &b);

  Cluster<T> mergeClustersMean(const Cluster<T> &a, const Cluster<T> &b);

public:
  Cluster<T> getMaxEntriesCluster();
  int getMaxClusterSize_Info();
  // void showClustersAndContent();
  std::vector<Cluster<T>> getClusters() const;
  int getNClusters() const;
  /**
   * @brief Set the distance cut. This allows an update and rerunning of the
   * vertexing then no vertex that fulfills the selection criteria is found.
   *
   * @param [in] cut_value Given in units of resolution.
   *
   */
  void updateDistanceCut(double cut_value);
  void setDebugLevel(int debug_level) { m_debug_level = debug_level; }

private:
  int m_debug_level = 0;
  double m_distance_cut = 3.0;
  std::vector<Cluster<T>> m_clusters;
};

/////////////////////////////////////////////
/////////////////////////////////////////////
////                CPP                   ///
/////////////////////////////////////////////
/////////////////////////////////////////////

template <class T>
Cluster<T>::Cluster()
    : m_n_entries(0), m_combined_value(-999.), m_combined_sigma(-999.) {}

template <class T> int Cluster<T>::getNEntries() const { return m_n_entries; }

template <class T> void Cluster<T>::addEntry(const T &entry) {
  m_entries.push_back(entry);
  m_n_entries++;
}

template <class T> std::vector<T> Cluster<T>::getEntries() const {
  return m_entries;
}

template <class T>
void Cluster<T>::addEntryVector(const std::vector<T> &entry_vector) {
  if (not entry_vector.empty()) {
    m_entries.insert(m_entries.end(), entry_vector.begin(), entry_vector.end());
  }
  m_n_entries = m_entries.size();
}

template <class T>
void Cluster<T>::setClusterValue(const std::vector<double> &v,
                                 const std::vector<double> &v_sigma) {
  m_combined_value_vector = v;
  m_combined_sigma_vector = v_sigma;
}

template <class T> std::vector<double> Cluster<T>::getValues() const {
  // return a warning when this value is a default value
  if (m_combined_value_vector.size() == 0) {
    if (m_debug_level > 0) {
      std::cout
          << "Cluster::getTime: ATTENTION, combi values are not initialized!"
          << std::endl;
    }
  }
  return m_combined_value_vector;
}

template <class T> std::vector<double> Cluster<T>::getSigmas() const {
  return m_combined_sigma_vector;
}

template <class T> void Cluster<T>::setMergeStatus(bool status) {
  m_was_merged = status;
}

template <class T> bool Cluster<T>::mergeStatus() const { return m_was_merged; }

template <class T> int Cluster<T>::getMergeIteration() const {
  return m_merge_iteration;
}

template <class T> void Cluster<T>::setMergeIteration(int iteration) {
  m_merge_iteration = iteration;
}

template <class T> bool Cluster<T>::containsUnknowns() const {
  return m_contains_unknowns;
}

template <class T> void Cluster<T>::setUnknownStatus(bool status) {
  m_contains_unknowns = status;
}

///////////////////////////////////////////////////
///////////////////////////////////////////////////

template <class T>
void ClusterCollection<T>::updateDistanceCut(double cut_value) {
  m_distance_cut = cut_value;
}

template <typename T> void ClusterCollection<T>::addCluster(Cluster<T> vx) {
  m_clusters.push_back(vx);
}

// for scalars
// template<typename T>
// double ClusterCollection<T>::getDistanceBetweenClusters(const Cluster<T>& a,
// const Cluster<T>& b) {
//   double distance = fabs(a.getValue() -
//   b.getValue())/sqrt(a.getSigma()*a.getSigma() + b.getSigma()*b.getSigma());
//   return distance;
// }

// for vectors
template <typename T>
double ClusterCollection<T>::getDistanceBetweenClusters(const Cluster<T> &a,
                                                        const Cluster<T> &b) {
  std::vector<double> a_values = a.getValues();
  std::vector<double> b_values = b.getValues();
  std::vector<double> a_sigmas = a.getSigmas();
  std::vector<double> b_sigmas = b.getSigmas();

  std::vector<double> distances(a_values.size());
  for (size_t i = 0; i < a_values.size(); i++) {
    double distance_i = 0;
    if (a_sigmas.at(i) < 0 or b_sigmas.at(i) < 0) {

    } else {
      distance_i = fabs(a_values.at(i) - b_values.at(i)) /
                   sqrt(a_sigmas.at(i) * a_sigmas.at(i) +
                        b_sigmas.at(i) * b_sigmas.at(i));
    }
    distances.at(i) = distance_i;
  }
  double distance2 = 0.;
  for (double d : distances) {
    distance2 += d * d;
  }
  return sqrt(distance2);
}

// for scalars
// template <class T>
// Cluster<T> ClusterCollection<T>::mergeClusters(const Cluster<T>& a, const
// Cluster<T>& b) {
//   Cluster<T> merged_cluster;
//   merged_cluster.addEntryVector(a.getEntries());
//   merged_cluster.addEntryVector(b.getEntries());
//
//
//   double sigma1 = a.getSigma();
//   double sigma2 = b.getSigma();
//   double new_cluster_time = (a.getValue()/(sigma1*sigma1) +
//   b.getValue()/(sigma2*sigma2)) / (1./(sigma1*sigma1) + 1./(sigma2*sigma2));
//   double new_cluster_sigma = sqrt((sigma2*sigma1)*(sigma2*sigma1) /
//   (sigma2*sigma2 + sigma1*sigma1)); int new_merge_iteration =
//   a.getMergeIteration() + b.getMergeIteration() + 1; if
//   (new_merge_iteration>4) {
//     new_cluster_sigma = sigma1 > sigma2 ?  sigma1 : sigma2;
//   }
//   merged_cluster.setClusterValue(new_cluster_time, new_cluster_sigma);
//   merged_cluster.setMergeStatus(true);
//   merged_cluster.setMergeIteration(new_merge_iteration);
//   return merged_cluster;
// }

// for vectors
template <class T>
Cluster<T> ClusterCollection<T>::mergeClusters(const Cluster<T> &a,
                                               const Cluster<T> &b) {
  Cluster<T> merged_cluster;
  merged_cluster.addEntryVector(a.getEntries());
  merged_cluster.addEntryVector(b.getEntries());

  std::vector<double> a_values = a.getValues();
  std::vector<double> b_values = b.getValues();
  std::vector<double> a_sigmas = a.getSigmas();
  std::vector<double> b_sigmas = b.getSigmas();

  std::vector<double> new_cluster_values(a_values.size());
  std::vector<double> new_cluster_sigmas(a_values.size());

  for (size_t i = 0; i < a_values.size(); i++) {
    double value1 = a_values.at(i);
    double value2 = b_values.at(i);
    double sigma1 = a_sigmas.at(i);
    double sigma2 = b_sigmas.at(i);
    double new_cluster_value =
        (value1 / (sigma1 * sigma1) + value2 / (sigma2 * sigma2)) /
        (1. / (sigma1 * sigma1) + 1. / (sigma2 * sigma2));
    double new_cluster_sigma = sqrt((sigma2 * sigma1) * (sigma2 * sigma1) /
                                    (sigma2 * sigma2 + sigma1 * sigma1));
    new_cluster_values.at(i) = new_cluster_value;
    new_cluster_sigmas.at(i) = new_cluster_sigma;
  }
  int new_merge_iteration = a.getMergeIteration() + b.getMergeIteration() + 1;
  merged_cluster.setClusterValue(new_cluster_values, new_cluster_sigmas);
  merged_cluster.setMergeStatus(true);
  merged_cluster.setMergeIteration(new_merge_iteration);
  return merged_cluster;
}

template <class T>
Cluster<T> ClusterCollection<T>::mergeClustersMean(const Cluster<T> &a,
                                                   const Cluster<T> &b) {
  Cluster<T> merged_cluster;
  merged_cluster.addEntryVector(a.getEntries());
  merged_cluster.addEntryVector(b.getEntries());

  std::vector<double> a_values = a.getValues();
  std::vector<double> b_values = b.getValues();
  std::vector<double> a_sigmas = a.getSigmas();
  std::vector<double> b_sigmas = b.getSigmas();

  std::vector<double> new_cluster_values(a_values.size());
  std::vector<double> new_cluster_sigmas(a_values.size());

  for (size_t i = 0; i < a_values.size(); i++) {
    double value1 = a_values.at(i);
    double value2 = b_values.at(i);
    double sigma1 = a_sigmas.at(i);
    double sigma2 = b_sigmas.at(i);
    // double new_cluster_value = (value1/(sigma1*sigma1) +
    // value2/(sigma2*sigma2)) / (1./(sigma1*sigma1) + 1./(sigma2*sigma2));
    double new_cluster_value = (value1 + value2) / 2.;
    double new_cluster_sigma = sqrt(sigma1 * sigma1 + sigma2 * sigma2);
    new_cluster_values.at(i) = new_cluster_value;
    new_cluster_sigmas.at(i) = new_cluster_sigma;
  }
  int new_merge_iteration = a.getMergeIteration() + b.getMergeIteration() + 1;
  merged_cluster.setClusterValue(new_cluster_values, new_cluster_sigmas);
  merged_cluster.setMergeStatus(true);
  merged_cluster.setMergeIteration(new_merge_iteration);
  return merged_cluster;
}

template <typename T>
void ClusterCollection<T>::doClustering(CH::ClusterAlgoType algo) {
  if (algo == CH::ClusterAlgoType::Eager) {
    for (const auto &clust : m_clusters) {
      if (clust.containsUnknowns()) {
        throw std::invalid_argument(
            "[ClusterCollection::doClustering] ERROR "
            "- eager clustering does not allow for unknown values");
      }
    }
    doEagerClustering();
  } else if (algo == CH::ClusterAlgoType::Simultaneous) {
    doSimultaneousClustering();
  } else if (algo == CH::ClusterAlgoType::SimultaneousMean) {
    doSimultaneousClusteringMean();
  } else {
    throw std::invalid_argument("[ClusterCollection::doClustering] ERROR "
                                "- chosen clustering algorithm does not exist");
  }
}

template <typename T> void ClusterCollection<T>::doEagerClustering() {
  // TODO case where I have 2 vertices in my collection
  if (m_debug_level > 0) {
    std::cout << "ClusterCollection::doTimeClustering" << std::endl;
  }
  double distance = 1.e30; // initial distance value, "far away"

  while (m_clusters.size() > 1) {
    int i0 = 0;
    int j0 = 0;
    if (m_debug_level > 0) {
      std::cout << "using " << m_clusters.size() << " vertices" << std::endl;
    }
    // find the two vertices that are closest to each other
    distance = getDistanceBetweenClusters(m_clusters.at(0), m_clusters.at(1));
    for (size_t i = 0; i < m_clusters.size(); i++) {
      for (size_t j = i + 1; j < m_clusters.size(); j++) {
        double current_distance =
            getDistanceBetweenClusters(m_clusters.at(i), m_clusters.at(j));
        if (current_distance <= distance) {
          distance = current_distance;
          i0 = i;
          j0 = j;
        }
      } // loop over j
    }   // loop over i
    if (m_debug_level > 0) {
      std::cout << "using vertex " << i0 << " and " << j0 << std::endl;
    }
    // now the closest two vertices are found and will be fused if cut fulfulled
    if (distance < m_distance_cut) {
      // Cluster<T> new_cluster =
      // m_clusters.at(i0).fuseWithAnotherCluster(m_clusters.at(j0));
      Cluster<T> new_cluster =
          mergeClusters(m_clusters.at(i0), m_clusters.at(j0));
      if (m_debug_level > 0) {
        std::cout << "starting to erase" << std::endl;
      }
      m_clusters.erase(m_clusters.begin() + j0);
      if (i0 < j0) {
        m_clusters.erase(m_clusters.begin() + i0);
      } else {
        m_clusters.erase(m_clusters.begin() + (i0 - 1));
      }
      if (m_debug_level > 0) {
        std::cout << "erase done" << std::endl;
      }
      m_clusters.push_back(new_cluster);
      if (m_debug_level > 0) {
        std::cout << "new cluster stored" << std::endl;
      }
    } else {
      break;
    }
  } // while loop
}

template <typename T> void ClusterCollection<T>::doSimultaneousClustering() {
  // TODO case where I have 2 vertices in my collection
  if (m_debug_level > 0) {
    std::cout << "ClusterCollection::doTimeClustering" << std::endl;
  }
  double distance = 1.e30; // initial distance value, "far away"

  while (m_clusters.size() > 1) {
    if (m_debug_level > 0) {
      std::cout << "entering while loop" << std::endl;
    }
    int i0 = 0;
    int j0 = 0;
    if (m_debug_level > 0) {
      std::cout << "using " << m_clusters.size() << " vertices" << std::endl;
    }
    // find the two vertices that are closest to each other
    distance = getDistanceBetweenClusters(m_clusters.at(0), m_clusters.at(1));
    if (m_debug_level > 0) {
      std::cout << "first vertices with distance: " << distance << std::endl;
    }
    for (size_t i = 0; i < m_clusters.size(); i++) {
      for (size_t j = i + 1; j < m_clusters.size(); j++) {
        if (m_clusters.at(i).mergeStatus() or m_clusters.at(j).mergeStatus()) {
          continue;
        }
        double current_distance =
            getDistanceBetweenClusters(m_clusters.at(i), m_clusters.at(j));
        if (current_distance <= distance) {
          distance = current_distance;
          i0 = i;
          j0 = j;
        }
      } // loop over j
    }   // loop over i
    if (m_debug_level > 0) {
      std::cout << "using vertex " << i0 << " and " << j0 << std::endl;
      std::cout << "with values: " << std::endl;
      for (const auto &cv : m_clusters.at(i0).getValues()) {
        std::cout << cv << std::endl;
      }
      std::cout << "and: " << std::endl;
      for (const auto &cv : m_clusters.at(j0).getValues()) {
        std::cout << cv << std::endl;
      }
      std::cout << "distance: " << distance
                << " at current cut: " << m_distance_cut << std::endl;
    }
    // now the closest two vertices are found and will be fused if cut passed
    if (distance < m_distance_cut and i0 != j0) {
      // Cluster<T> new_cluster =
      // m_clusters.at(i0).fuseWithAnotherCluster(m_clusters.at(j0));
      Cluster<T> new_cluster =
          mergeClusters(m_clusters.at(i0), m_clusters.at(j0));
      // new_cluster.setUnknownStatus(false);
      if (m_debug_level > 0) {
        std::cout << "starting to erase" << std::endl;
      }
      m_clusters.erase(m_clusters.begin() + j0);

      if (i0 < j0) {
        m_clusters.erase(m_clusters.begin() + i0);
      } else {
        m_clusters.erase(m_clusters.begin() + (i0 - 1));
      }

      if (m_debug_level > 0) {
        std::cout << "erase done" << std::endl;
      }
      m_clusters.push_back(new_cluster);
      if (m_debug_level > 0) {
        std::cout << "new cluster stored" << std::endl;
      }
    } else {
      // if there is a cluster that was merged
      if (std::find_if(m_clusters.begin(), m_clusters.end(),
                       [](const Cluster<T> &c) { return c.mergeStatus(); }) !=
          m_clusters.end()) {
        // reset each status to false
        std::for_each(m_clusters.begin(), m_clusters.end(), [](Cluster<T> &c) {
          c.setMergeStatus(false);
          // also clusters with unknown are allowed to get deleted at this point
          // c.setUnknownStatus(false);
        });
      } else {
        // if not, this is the end of the clustering
        break;
      }
    }
  } // while loop
}

template <typename T>
void ClusterCollection<T>::doSimultaneousClusteringMean() {
  // TODO case where I have 2 vertices in my collection
  if (m_debug_level > 0) {
    std::cout << "ClusterCollection::doTimeClustering" << std::endl;
  }
  double distance = 1.e30; // initial distance value, "far away"

  while (m_clusters.size() > 1) {
    if (m_debug_level > 0) {
      std::cout << "entering while loop" << std::endl;
    }
    int i0 = 0;
    int j0 = 0;
    if (m_debug_level > 0) {
      std::cout << "using " << m_clusters.size() << " vertices" << std::endl;
    }
    // find the two vertices that are closest to each other
    distance = getDistanceBetweenClusters(m_clusters.at(0), m_clusters.at(1));
    if (m_debug_level > 0) {
      std::cout << "first vertices with distance: " << distance << std::endl;
    }
    for (size_t i = 0; i < m_clusters.size(); i++) {
      for (size_t j = i + 1; j < m_clusters.size(); j++) {
        if (m_clusters.at(i).mergeStatus() or m_clusters.at(j).mergeStatus()) {
          continue;
        }
        double current_distance =
            getDistanceBetweenClusters(m_clusters.at(i), m_clusters.at(j));
        if (current_distance <= distance) {
          distance = current_distance;
          i0 = i;
          j0 = j;
        }
      } // loop over j
    }   // loop over i
    if (m_debug_level > 0) {
      std::cout << "using vertex " << i0 << " and " << j0 << std::endl;
      std::cout << "with values: " << std::endl;
      for (const auto &cv : m_clusters.at(i0).getValues()) {
        std::cout << cv << std::endl;
      }
      std::cout << "and: " << std::endl;
      for (const auto &cv : m_clusters.at(j0).getValues()) {
        std::cout << cv << std::endl;
      }
      std::cout << "distance: " << distance
                << " at current cut: " << m_distance_cut << std::endl;
    }
    // now the closest two vertices are found and will be fused if cut passed
    if (distance < m_distance_cut and i0 != j0) {
      // Cluster<T> new_cluster =
      // m_clusters.at(i0).fuseWithAnotherCluster(m_clusters.at(j0));
      Cluster<T> new_cluster =
          mergeClustersMean(m_clusters.at(i0), m_clusters.at(j0));
      // new_cluster.setUnknownStatus(false);
      if (m_debug_level > 0) {
        std::cout << "starting to erase" << std::endl;
      }
      m_clusters.erase(m_clusters.begin() + j0);

      if (i0 < j0) {
        m_clusters.erase(m_clusters.begin() + i0);
      } else {
        m_clusters.erase(m_clusters.begin() + (i0 - 1));
      }

      if (m_debug_level > 0) {
        std::cout << "erase done" << std::endl;
      }
      m_clusters.push_back(new_cluster);
      if (m_debug_level > 0) {
        std::cout << "new cluster stored" << std::endl;
      }
    } else {
      // if there is a cluster that was merged
      if (std::find_if(m_clusters.begin(), m_clusters.end(),
                       [](const Cluster<T> &c) { return c.mergeStatus(); }) !=
          m_clusters.end()) {
        // reset each status to false
        std::for_each(m_clusters.begin(), m_clusters.end(), [](Cluster<T> &c) {
          c.setMergeStatus(false);
          // also clusters with unknown are allowed to get deleted at this point
          // c.setUnknownStatus(false);
        });
      } else {
        // if not, this is the end of the clustering
        break;
      }
    }
  } // while loop
}

template <typename T> Cluster<T> ClusterCollection<T>::getMaxEntriesCluster() {
  // find the vertex with a maximum amount of hits clustered in it
  int max_n_hits = 0;
  Cluster<T> max_n_vertex;
  for (auto vx : m_clusters) {
    int current_n = vx.getNEntries();
    if (current_n > max_n_hits) {
      max_n_hits = current_n;
      max_n_vertex = vx;
    }
  }
  // count how many vertices have max_n_vertex entries
  int count = 0;
  for (auto vx : m_clusters) {
    int current_n = vx.getNEntries();
    if (current_n == max_n_hits) {
      count++;
    }
  }
  // if two (or more) vertices have the max n of hits, I can't decide, so return
  // default
  if (count > 1) {
    Cluster<T> default_vertex;
    return default_vertex;
  }
  return max_n_vertex;
}

template <typename T> int ClusterCollection<T>::getMaxClusterSize_Info() {
  // find the vertex with a maximum amount of hits clustered in it
  // and return thisn umber
  int max_n_hits = 0;
  for (auto vx : m_clusters) {
    int current_n = vx.getNEntries();
    if (current_n > max_n_hits) {
      max_n_hits = current_n;
    }
  }
  return max_n_hits;
}

template <typename T>
std::vector<Cluster<T>> ClusterCollection<T>::getClusters() const {
  return m_clusters;
}

template <typename T> int ClusterCollection<T>::getNClusters() const {
  return static_cast<int>(m_clusters.size());
}

#endif // CLUSTERINGHELPER_H
